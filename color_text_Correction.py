from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

class NotreAppli(App):
    def build(self):
        self.box1 = BoxLayout()
        self.box1.rows = 2

        self.gridGauche = GridLayout(rows=2)
        self.gridDroite = GridLayout(cols=2)

        self.label = Label(text="Entrez du texte")
        self.gridGauche.add_widget(self.label)

        self.input = TextInput()
        self.input.bind(text=self.text)
        self.gridGauche.add_widget(self.input)

        # alignement des boutons en vertical
        self.gridDroite.size_hint_y = 0.9
        # Marges intérieures des 2 grids
        self.gridGauche.padding = 100
        self.gridDroite.padding = 20
        # espacement entre les boutons
        self.gridDroite.spacing = 30

        # instanciation des 5 boutons
        self.btn_Rouge = self.bouton("Rouge", "red")
        self.btn_Bleu = self.bouton("Bleu", "blue")
        self.btn_Jaune = self.bouton("Jaune", "yellow")
        self.btn_Rose = self.bouton("Rose", "pink")
        self.btn_Marron = self.bouton("Marron", "brown")
        self.btn_Vert = self.bouton("Vert", "green")

        self.box1.add_widget(self.gridGauche)
        self.box1.add_widget(self.gridDroite)

        return self.box1


    """
    Fonction qui change la couleur du text du label
    """
    def changeCouleur(self, button):
        self.label.color = button.background_color


    """
    Fonction qui affecte au label la valeur du textInput
    """
    def text(self, instance, args):
        self.label.text = instance.text


    """
    Fonction qui créer un bouton avec du texte et une couleur de font, le bind a changeCouleur et l'ajoute au gridDroite
    """
    def bouton(self, texte, couleur):
        self.button = Button(text = texte, background_color = couleur, background_normal="")
        self.button.bind(on_press = self.changeCouleur)
        self.gridDroite.add_widget(self.button)

if __name__ == "__main__":
    NotreAppli().run()
