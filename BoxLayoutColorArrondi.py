from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.checkbox import CheckBox
from kivy.graphics import Color, RoundedRectangle


class ColorBoxLayoutApp(App):
    def build(self):
        """
        Fonction de construction de l'interface utilisateur de l'application.

        Returns:
            BoxLayout: La disposition principale de l'application.
        """
        # Créer une boîte (BoxLayout)
        box_layout = BoxLayout(orientation="vertical")

        # Ajouter un Label à la boîte
        label = Label(text="Ceci est une BoxLayout Colorer Arrondi")

        # Ajouter la boîte et le label à la fenêtre
        box_layout.add_widget(label)

        # Ajouter une couleur rouge à la boîte avec des coins arrondis
        with box_layout.canvas.before:
            Color(1, 0, 0, 1)  # Définir la couleur RVB et l'opacité
            self.rect = RoundedRectangle(
                size=box_layout.size, pos=box_layout.pos, radius=[10, 10, 10, 10]
            )

        # Mettre à jour la couleur si la taille de la boîte change
        box_layout.bind(size=self._update_rect, pos=self._update_rect)

        return box_layout

    def _update_rect(self, instance, value):
        """
        Met à jour la taille et la position du rectangle en fonction de la boîte.

        Args:
            instance (BoxLayout): L'instance de la boîte.
            value: La nouvelle valeur de la taille ou de la position.
        """
        # Mettre à jour la taille et la position du rectangle en fonction de la boîte
        self.rect.pos = instance.pos
        self.rect.size = instance.size


if __name__ == "__main__":
    ColorBoxLayoutApp().run()
