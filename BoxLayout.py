from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.checkbox import CheckBox
from kivy.uix.button import Button
from kivy.graphics import Color, RoundedRectangle
from kivy.uix.textinput import TextInput


class TodoApp(App):
    def build(self):
        # Création de la boîte principale (BoxLayout) avec coins arrondis
        self.main_layout = self.create_rounded_box_layout()

        # Ajout de la boîte d'ajout de tâches à la fenêtre
        self.add_task_input_layout()

        # Ajout de la boîte pour afficher les tâches
        self.add_task_display_layout()

        return self.main_layout

    def create_rounded_box_layout(self):
        """
        Crée et retourne une boîte (BoxLayout) avec coins arrondis.

        Returns:
            RoundedBoxLayout: Boîte avec coins arrondis.
        """
        return RoundedBoxLayout(orientation="vertical")

    def add_task_input_layout(self):
        """
        Ajoute la boîte d'ajout de tâches à la boîte principale.
        """
        # Création de la boîte d'ajout de tâches (GridLayout)
        input_layout = GridLayout(rows=1, size_hint_y=None, height=50)

        # Ajout de la couleur à la boîte d'ajout de tâches
        with input_layout.canvas.before:
            Color(0.8, 0.8, 0.8, 1)  # Couleur gris clair
            self.input_rect = RoundedRectangle(
                size=input_layout.size, pos=input_layout.pos, radius=[15, 15, 15, 15]
            )

        # Champ de saisie pour la nouvelle tâche
        self.task_input = TextInput(hint_text="Tâche à ajouter", multiline=False)

        # Bouton pour ajouter la tâche
        add_button = Button(text="Ajouter", on_press=self.add_task)

        # Ajout du champ de saisie et du bouton à la boîte d'ajout de tâches
        input_layout.add_widget(self.task_input)
        input_layout.add_widget(add_button)

        # Ajout de la boîte d'ajout de tâches à la boîte principale
        self.main_layout.add_widget(input_layout)

    def add_task_display_layout(self):
        """
        Ajoute la boîte pour afficher les tâches à la boîte principale.
        """
        # Création de la boîte pour afficher les tâches (GridLayout)
        display_layout = GridLayout(cols=2, spacing=10)

        # Ajout de la couleur à la boîte d'affichage de tâches
        with display_layout.canvas.before:
            Color(1, 1, 1, 1)  # Couleur blanche
            self.display_rect = RoundedRectangle(
                size=display_layout.size,
                pos=display_layout.pos,
                radius=[15, 15, 15, 15],
            )

        # Ajout de la boîte d'affichage de tâches à la boîte principale
        self.main_layout.add_widget(display_layout)

        # Création de la boîte pour les tâches à faire (GridLayout)
        todo_layout = self.create_task_layout("À faire")

        # Création de la boîte pour les tâches effectuées (GridLayout)
        done_layout = self.create_task_layout("Effectuées")

        # Ajout des boîtes de tâches à la boîte d'affichage de tâches
        display_layout.add_widget(todo_layout)
        display_layout.add_widget(done_layout)

    def create_task_layout(self, title):
        """
        Crée et retourne une boîte de tâches (GridLayout).

        Args:
            title (str): Le titre de la boîte de tâches.

        Returns:
            GridLayout: Boîte de tâches.
        """
        # Crée et retourne une boîte de tâches (GridLayout)
        task_layout = GridLayout(cols=1, spacing=10, size_hint_y=None, height=200)

        # Label pour le titre de la boîte de tâches
        title_label = Label(text=title, size_hint_y=None, height=30)

        # Ajout de la couleur à la boîte de tâches
        with task_layout.canvas.before:
            Color(0.9, 0.9, 0.9, 1)  # Couleur gris clair
            self.task_rect = RoundedRectangle(
                size=task_layout.size, pos=task_layout.pos, radius=[15, 15, 15, 15]
            )

        # Ajout du titre à la boîte de tâches
        task_layout.add_widget(title_label)

        return task_layout

    def add_task(self, instance):
        """
        Ajoute une nouvelle tâche à la boîte de tâches à faire.

        Args:
            instance: L'instance de l'élément déclenchant l'événement (bouton).
        """
        # Récupération de la tâche depuis le champ de saisie
        new_task = self.task_input.text

        # Création d'un Label pour afficher la nouvelle tâche
        task_label = Label(text=new_task)

        # Ajout du Label à la boîte de tâches à faire
        todo_layout = self.main_layout.children[1].children[0]
        todo_layout.add_widget(task_label)

        # Effacer le champ de saisie après l'ajout de la tâche
        self.task_input.text = ""

    def on_main_layout_size(self, instance, value):
        """
        Met à jour la taille, la position et les coins des rectangles en fonction de la boîte principale.

        Args:
            instance (RoundedBoxLayout): L'instance de la boîte principale.
            value: La nouvelle valeur de la taille ou de la position.
        """
        # Met à jour la taille, la position et les coins des rectangles en fonction de la boîte principale
        self.input_rect.pos = self.main_layout.children[0].pos
        self.input_rect.size = self.main_layout.children[0].size
        self.display_rect.pos = self.main_layout.children[1].pos
        self.display_rect.size = self.main_layout.children[1].size


class RoundedBoxLayout(BoxLayout):
    def __init__(self, **kwargs):
        """
        Initialise une boîte avec coins arrondis.

        Args:
            **kwargs: Arguments supplémentaires à passer à la classe mère.
        """
        super(RoundedBoxLayout, self).__init__(**kwargs)

        # Ajoute la couleur blanche à la boîte avec coins arrondis
        with self.canvas.before:
            Color(1, 1, 1, 1)  # Définir la couleur RVB et l'opacité
            self.rect = RoundedRectangle(
                size=self.size, pos=self.pos, radius=[15, 15, 15, 15]
            )

        # Met à jour la couleur et les coins si la taille de la boîte change
        self.bind(size=self.update_rect, pos=self.update_rect)

    def update_rect(self, instance, value):
        """
        Met à jour la taille, la position et les coins du rectangle en fonction de la boîte.

        Args:
            instance (RoundedBoxLayout): L'instance de la boîte.
            value: La nouvelle valeur de la taille ou de la position.
        """
        # Met à jour la taille, la position et les coins du rectangle en fonction de la boîte
        self.rect.pos = instance.pos
        self.rect.size = instance.size
        self.rect.radius = [15, 15, 15, 15]


if __name__ == "__main__":
    TodoApp().run()
