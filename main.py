from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.graphics import Color

class ColorChangingApp(App):
    def build(self):
        # Crée une disposition en grille avec 2 colonnes et des marges et espacements spécifiés
        layout = GridLayout(cols=2, spacing=55, padding=65)

        # Crée le champ de saisie de texte initial avec un texte par défaut
        self.text_input = TextInput(text='Entrez du texte', multiline=False)
        layout.add_widget(self.text_input)

        # Crée le label initial avec un texte par défaut
        self.label = Label(text='Entrez du texte')
        layout.add_widget(self.label)

        # Crée les boutons pour les couleurs et les ajoute à la disposition
        colors = ['Rouge', 'Bleu', 'Vert', 'Jaune', 'Rose', 'Marron']
        for color in colors:
            btn = Button(text=color)
            btn.bind(on_press=self.change_color)  # Associe la fonction de changement de couleur au clic
            btn.bind(on_release=self.restore_color)  # Associe la fonction de restauration de couleur au relâchement
            layout.add_widget(btn)

        # Associe la fonction de mise à jour du label au changement de texte dans le TextInput
        self.text_input.bind(text=self.on_text_input_change)

        return layout

    def on_text_input_change(self, instance, value):
        """
        Fonction appelée lors de la modification du texte dans le TextInput.

        :param instance: Instance du widget TextInput
        :param value: Nouvelle valeur du texte
        """
        self.label.text = value

    def change_color(self, instance):
        """
        Fonction appelée lors du clic sur un bouton de couleur pour changer la couleur du label.

        :param instance: Instance du bouton qui a été cliqué
        """
        color_name = instance.text.lower()
        colors_dict = {'rouge': (1, 0, 0, 1), 'bleu': (0, 0, 1, 1), 'vert': (0, 1, 0, 1),
                       'jaune': (1, 1, 0, 1), 'rose': (1, 0, 1, 1), 'marron': (0.5, 0.25, 0, 1)}

        if color_name in colors_dict:
            self.label.color = colors_dict[color_name]
            instance.background_color = colors_dict[color_name]

    def restore_color(self, instance):
        """
        Fonction appelée lors du relâchement d'un bouton de couleur pour restaurer sa couleur par défaut.

        :param instance: Instance du bouton qui a été relâché
        """
        instance.background_color = (1, 1, 1, 1)  # Restaurer la couleur par défaut du bouton

if __name__ == '__main__':
    ColorChangingApp().run()
