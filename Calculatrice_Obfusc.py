from kivy .app import App #line:1
from kivy .uix .floatlayout import FloatLayout #line:2
from kivy .uix .gridlayout import GridLayout #line:3
from kivy .uix .button import Button #line:4
from kivy .uix .label import Label #line:5
from kivy .graphics import Color ,Rectangle #line:6
from kivy .storage .jsonstore import JsonStore #line:7
class CalculatriceApp (App ):#line:10
    """
           Fonction qui construit l'interface de la calculatrice.mais obfucator

           Returns:
               GridLayout: La disposition principale de l'application.
           """
    def build (O0OOO0000O0O0O0O0 ):#line:11
        O00O000O0OO0OO0OO =GridLayout (cols =1 ,spacing =10 ,padding =10 )#line:13
        O0OOO0000O0O0O0O0 .result_store =JsonStore ('results.json')#line:15
        O0OOO0000O0O0O0O0 .last_calculation_label =Label (font_size =20 ,halign ='right',valign ='center',text ='',size_hint_y =None ,height =30 )#line:19
        O00O000O0OO0OO0OO .add_widget (O0OOO0000O0O0O0O0 .last_calculation_label )#line:20
        O0OOO0000O0O0O0O0 .second_last_calculation_label =Label (font_size =20 ,halign ='right',valign ='center',text ='',size_hint_y =None ,height =30 )#line:24
        O00O000O0OO0OO0OO .add_widget (O0OOO0000O0O0O0O0 .second_last_calculation_label )#line:25
        O0OOO0000O0O0O0O0 .label =Label (font_size =32 ,halign ='right',valign ='center',text ='0',size_hint_y =None ,height =75 )#line:28
        O0OOO0000O0O0O0O0 .label1 =Label (font_size =32 ,halign ='right',valign ='center',text ='0',size_hint_y =None ,height =75 )#line:29
        O0OOO0000O0O0O0O0 .label .text_size =(None ,O0OOO0000O0O0O0O0 .label .height ,)#line:32
        O0OOO0000O0O0O0O0 .label .color ='green'#line:33
        O00O000O0OO0OO0OO .add_widget (O0OOO0000O0O0O0O0 .label )#line:34
        OO0OO0000OO0O00OO =GridLayout (cols =4 ,spacing =10 )#line:37
        OO0O0OOO00O0OOOOO =FloatLayout ()#line:38
        O0O000OOOO00000OO =['7','8','9','÷','4','5','6','x','1','2','3','-','C','0','.','+','=']#line:46
        for OO00OO0O00O0OOO00 in O0O000OOOO00000OO :#line:49
            O0O0OO00000O0O0OO =Button (text =OO00OO0O00O0OOO00 ,pos_hint ={'center_x':0.5 ,'center_y':0.5 },background_normal ='',background_color =[0.1 ,0.1 ,0.1 ,1 ])#line:51
            O0O0OO00000O0O0OO .font_size =30 #line:52
            O0O0OO00000O0O0OO .bind (on_press =O0OOO0000O0O0O0O0 .on_button_press )#line:53
            if O0O0OO00000O0O0OO .text =='=':#line:54
                O0O0OO00000O0O0OO .color ='green'#line:55
                OO0O0OOO00O0OOOOO .size_hint_y =0.3 #line:56
                OO0O0OOO00O0OOOOO .add_widget (O0O0OO00000O0O0OO )#line:57
            elif O0O0OO00000O0O0OO .text =='C':#line:58
                 O0O0OO00000O0O0OO .color ='red'#line:59
                 O0O0OO00000O0O0OO .border_radius =[10 ,10 ,10 ,10 ]#line:60
                 OO0OO0000OO0O00OO .add_widget (O0O0OO00000O0O0OO )#line:61
            else :#line:62
                O0O0OO00000O0O0OO .border_radius =[10 ,10 ,10 ,10 ]#line:63
                OO0OO0000OO0O00OO .add_widget (O0O0OO00000O0O0OO )#line:64
        with O0OOO0000O0O0O0O0 .label .canvas .before :#line:68
            Color (1 ,1 ,0 ,0.8 )#line:69
            O0OOO0000O0O0O0O0 .rect =Rectangle (pos =O0OOO0000O0O0O0O0 .label .pos ,size =O0OOO0000O0O0O0O0 .label .size )#line:70
        O00O000O0OO0OO0OO .add_widget (OO0OO0000OO0O00OO )#line:73
        O00O000O0OO0OO0OO .add_widget (OO0O0OOO00O0OOOOO )#line:74
        O0OOO0000O0O0O0O0 .label .bind (pos =O0OOO0000O0O0O0O0 .update_rect_pos )#line:77
        O0OOO0000O0O0O0O0 .label .bind (size =O0OOO0000O0O0O0O0 .update_rect_size )#line:78
        O0OOO0000O0O0O0O0 .calculation_history =[]#line:81
        O0OOO0000O0O0O0O0 .evaluated =False #line:84
        return O00O000O0OO0OO0OO #line:86
    def update_rect_pos (O0OO00000O000OO00 ,O00O0OO0O0O0O0OO0 ,OOO0OOO000OO0O0OO ):#line:88
        O0OO00000O000OO00 .rect .pos =(O00O0OO0O0O0O0OO0 .x ,O00O0OO0O0O0O0OO0 .y )#line:90
    def update_rect_size (OOOOOOO0OOO00OO00 ,OOO0O00OOO000OO00 ,OOOO0O00O00OO0OO0 ):#line:92
        OOOOOOO0OOO00OO00 .rect .size =(OOO0O00OOO000OO00 .width ,OOO0O00OOO000OO00 .height )#line:94
    def on_button_press (OO00O00O0OO0O0O00 ,OOO0OO0O0OO0OO00O ):#line:96
        O0O00OOOOOO00OOO0 =OO00O00O0OO0O0O00 .label .text #line:97
        if OO00O00O0OO0O0O00 .evaluated :#line:99
            OO00O00O0OO0O0O00 .label .text =''#line:101
            OO00O00O0OO0O0O00 .evaluated =False #line:102
        if OOO0OO0O0OO0OO00O .text =='C':#line:104
            OO00O00O0OO0O0O00 .label .text ='0'#line:106
        if OOO0OO0O0OO0OO00O .text =='.':#line:107
            if '.'not in O0O00OOOOOO00OOO0 :#line:108
                OO00O00O0OO0O0O00 .label .text +='.'#line:109
        elif OOO0OO0O0OO0OO00O .text =='=':#line:110
            try :#line:112
                O0O00O0O000000OOO =str (eval (O0O00OOOOOO00OOO0 ))#line:113
                OO00O00O0OO0O0O00 .calculation_history .append (f"{O0O00OOOOOO00OOO0} = {O0O00O0O000000OOO}")#line:115
                OO00O00O0OO0O0O00 .calculation_history .append (f"{O0O00OOOOOO00OOO0} = {O0O00O0O000000OOO}")#line:116
                OOOOOOOOO0OO0O0OO =f"result_{len(OO00O00O0OO0O0O00.result_store) + 1}"#line:117
                OO00O00O0OO0O0O00 .result_store .put (OOOOOOOOO0OO0O0OO ,expression =O0O00OOOOOO00OOO0 ,result =O0O00O0O000000OOO )#line:118
                if len (OO00O00O0OO0O0O00 .calculation_history )>3 :#line:119
                    OO00O00O0OO0O0O00 .calculation_history =OO00O00O0OO0O0O00 .calculation_history [-3 :]#line:121
                if len (OO00O00O0OO0O0O00 .calculation_history )>=3 :#line:123
                    OO00O00O0OO0O0O00 .last_calculation_label .text =f'Avant-dernier calcul: {OO00O00O0OO0O0O00.calculation_history[-3]}'#line:124
                OO00O00O0OO0O0O00 .second_last_calculation_label .text =f'Dernier calcul: {OO00O00O0OO0O0O00.calculation_history[-2]}'#line:125
                OO00O00O0OO0O0O00 .label .text =O0O00O0O000000OOO #line:126
                OO00O00O0OO0O0O00 .evaluated =True #line:127
            except Exception as O00000OOO000O000O :#line:128
                OO00O00O0OO0O0O00 .label .text ='Error'#line:129
        elif OOO0OO0O0OO0OO00O .text .isdigit ():#line:130
            if O0O00OOOOOO00OOO0 =='0':#line:132
                OO00O00O0OO0O0O00 .label .text =OOO0OO0O0OO0OO00O .text #line:133
            else :#line:134
                OO00O00O0OO0O0O00 .label .text +=OOO0OO0O0OO0OO00O .text #line:135
        elif OOO0OO0O0OO0OO00O .text in ['+','-','*','÷','x']:#line:136
            OO00O00O0OO0O0O00 .label .text =O0O00OOOOOO00OOO0 +OOO0OO0O0OO0OO00O .text if OOO0OO0O0OO0OO00O .text !='x'else O0O00OOOOOO00OOO0 +'*'#line:138
            OO00O00O0OO0O0O00 .evaluated =False #line:139
if __name__ =='__main__':#line:143
    CalculatriceApp ().run ()#line:144
