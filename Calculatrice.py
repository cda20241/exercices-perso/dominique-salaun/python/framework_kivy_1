from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.graphics import Color, Rectangle
from kivy.storage.jsonstore import JsonStore


class CalculatriceApp(App):
    def build(self):
        """
        Fonction qui construit l'interface de la calculatrice.

        Returns:
            GridLayout: La disposition principale de l'application.
        """
        # Configuration de la disposition principale de la calculatrice
        main_layout = GridLayout(cols=1, spacing=10, padding=10)
        # Création de la base de données JSON
        self.result_store = JsonStore("results.json")

        # Ajout du label qui garde le dernier calcul
        self.last_calculation_label = Label(
            font_size=20,
            halign="right",
            valign="center",
            text="",
            size_hint_y=None,
            height=30,
        )
        main_layout.add_widget(self.last_calculation_label)

        # Ajout du label qui garde l'avant-dernier calcul
        self.second_last_calculation_label = Label(
            font_size=20,
            halign="right",
            valign="center",
            text="",
            size_hint_y=None,
            height=30,
        )
        main_layout.add_widget(self.second_last_calculation_label)

        # Ajout du label qui occupe toute la première ligne
        self.label = Label(
            font_size=32,
            halign="right",
            valign="center",
            text="0",
            size_hint_y=None,
            height=75,
        )
        self.label1 = Label(
            font_size=32,
            halign="right",
            valign="center",
            text="0",
            size_hint_y=None,
            height=75,
        )

        # Ajustement de text_size pour que le texte s'écrive de droite à gauche
        self.label.text_size = (
            None,
            self.label.height,
        )
        self.label.color = "green"
        main_layout.add_widget(self.label)

        # Configuration de la disposition pour les boutons
        button_layout = GridLayout(cols=4, spacing=10)
        button_layout1 = FloatLayout()

        buttons = [
            "7",
            "8",
            "9",
            "÷",
            "4",
            "5",
            "6",
            "x",
            "1",
            "2",
            "3",
            "-",
            "C",
            "0",
            ".",
            "+",
            "=",
        ]

        # Ajout des boutons à la disposition à partir de la deuxième ligne
        for button_text in buttons:
            button = Button(
                text=button_text,
                pos_hint={"center_x": 0.5, "center_y": 0.5},
                background_normal="",
                background_color=[0.1, 0.1, 0.1, 1],
            )
            button.font_size = 30
            button.bind(on_press=self.on_button_press)
            if button.text == "=":
                button.color = "green"
                button_layout1.size_hint_y = 0.3
                button_layout1.add_widget(button)
            elif button.text == "C":
                button.color = "red"
                button.border_radius = [10, 10, 10, 10]
                button_layout.add_widget(button)
            else:
                button.border_radius = [
                    10,
                    10,
                    10,
                    10,
                ]  # Vous pouvez ajuster ces valeurs selon vos préférences
                button_layout.add_widget(button)

        # Ajout d'un rectangle blanc en arrière-plan du label
        with self.label.canvas.before:
            Color(1, 1, 0, 0.8)  # Blanc (RGBA)
            self.rect = Rectangle(pos=self.label.pos, size=self.label.size)

        # Ajout de la disposition des boutons à la disposition principale
        main_layout.add_widget(button_layout)
        main_layout.add_widget(button_layout1)

        # Mettre à jour la position et la taille du rectangle lorsque la position du label change
        self.label.bind(pos=self.update_rect_pos)
        self.label.bind(size=self.update_rect_size)

        # Liste pour stocker l'historique des calculs
        self.calculation_history = []

        # Variable pour suivre si l'évaluation a été effectuée
        self.evaluated = False

        return main_layout

    def update_rect_pos(self, instance, value):
        """
        Fonction appelée lorsqu'il y a un changement de position du label.

        Args:
            instance (Label): L'instance du label.
            value: La nouvelle valeur de la position.
        """
        # Mettre à jour la position du rectangle en fonction de la position de la première ligne
        self.rect.pos = (instance.x, instance.y)

    def update_rect_size(self, instance, value):
        """
        Fonction appelée lorsqu'il y a un changement de taille du label.

        Args:
            instance (Label): L'instance du label.
            value: La nouvelle valeur de la taille.
        """
        # Mettre à jour la taille du rectangle en fonction de la taille de la première ligne
        self.rect.size = (instance.width, instance.height)

    def on_button_press(self, instance):
        """
        Fonction qui détecte le bouton appuyé, effectue le calcul en utilisant eval(),
        modifie les caractères 'x' en '*', '÷' en '/', permet de réinitialiser avec 'C', et détecte
        si +-*/ après l'évaluation pour continuer le calcul.

        Args:
            instance: L'instance du bouton appuyé.
        """

        current_text = self.label.text

        if self.evaluated:
            # Réinitialiser le texte si l'évaluation a été effectuée
            self.label.text = ""
            self.evaluated = False

        if instance.text == "C":
            # Effacer le texte
            self.label.text = "0"
        if instance.text == ".":
            if "." not in current_text:
                self.label.text += "."
        if instance.text == "÷":
            if "." not in current_text:
                self.label.text += "/"
        elif instance.text == "=":
            # Évaluer et afficher le résultat
            try:
                result = str(eval(current_text))
                # Ajouter le calcul actuel, le dernier calcul, et le résultat à l'historique
                self.calculation_history.append(f"{current_text} = {result}")
                self.calculation_history.append(f"{current_text} = {result}")
                result_key = f"result_{len(self.result_store) + 1}"
                self.result_store.put(
                    result_key, expression=current_text, result=result
                )
                if len(self.calculation_history) > 3:
                    # Garder uniquement les trois derniers éléments dans l'historique
                    self.calculation_history = self.calculation_history[-3:]
                # Mettre à jour les labels d'historique
                if len(self.calculation_history) >= 3:
                    self.last_calculation_label.text = (
                        f"Avant-dernier calcul: {self.calculation_history[-3]}"
                    )
                self.second_last_calculation_label.text = (
                    f"Dernier calcul: {self.calculation_history[-2]}"
                )
                self.label.text = result
                self.evaluated = True
            except Exception as e:
                self.label.text = "Erreur"
        elif instance.text.isdigit():
            # Ajouter le texte du bouton au calcul en cours
            if current_text == "0":
                self.label.text = instance.text
            else:
                self.label.text += instance.text
        elif instance.text in ["+", "-", "*", "÷", "x", "/"]:
            # Conserver le résultat et ajouter l'opérateur
            self.label.text = (
                current_text + instance.text
                if instance.text != "x"
                else current_text + "*"
            )
            self.evaluated = False


if __name__ == "__main__":
    CalculatriceApp().run()
