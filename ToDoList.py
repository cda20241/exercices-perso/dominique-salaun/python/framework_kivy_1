from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.checkbox import CheckBox
from kivy.storage.jsonstore import JsonStore
from kivy.graphics import Color, RoundedRectangle
import os


class RoundedColorBoxLayout(BoxLayout):
    def __init__(self, color=(0.2, 0.2, 0.2, 1), locked=False, **kwargs):
        """
        Initialise une boîte avec coins arrondis et couleur personnalisée.

        :param color: Couleur de fond de la boîte (par défaut: gris foncé)
        :param locked: Booléen indiquant si la boîte est verrouillée (par défaut: False)
        :param kwargs: Arguments supplémentaires pour la classe parente
        """
        super(RoundedColorBoxLayout, self).__init__(**kwargs)
        self.widget_color = color
        self.locked = locked  # Nouvel attribut de verrouillage

        with self.canvas.before:
            Color(*self.widget_color)
            self.rect = RoundedRectangle(pos=self.pos, radius=[10, 10, 10, 10])

        self.bind(size=self._update_rect, pos=self._update_rect)

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size

    minimum_size = "0dp"


class ColoredTextInput(TextInput):
    """
    Classe TextInput avec une couleur de fond personnalisée.
    """

    def __init__(self, **kwargs):
        super(ColoredTextInput, self).__init__(**kwargs)
        self.widget_color = (1, 1, 1, 1)
        self.background_color = self.widget_color


class ColoredButton(Button):
    """
    Classe Button avec une couleur de fond personnalisée.
    """

    def __init__(self, **kwargs):
        super(ColoredButton, self).__init__(**kwargs)
        self.widget_color = (1, 0, 0.2, 1)
        self.background_color = self.widget_color


class ColoredCheckBox(CheckBox):
    """
    Classe CheckBox avec une couleur de fond personnalisée.
    """

    def __init__(self, **kwargs):
        super(ColoredCheckBox, self).__init__(**kwargs)
        self.widget_color = (0, 1, 0, 1)
        self.background_color = self.widget_color


class ColoredLabel(Label):
    """
    Classe Label avec une couleur de texte personnalisée.
    """

    def __init__(self, **kwargs):
        super(ColoredLabel, self).__init__(**kwargs)
        self.widget_color = (1, 1, 1, 1)
        self.color = self.widget_color


class TodoListApp(App):
    def build(self):
        """
        Fonction de construction de l'interface graphique de l'application.

        :return: Retourne la disposition principale de l'interface graphique.
        """
        # Initialisation du titre de l'application et du chemin du fichier JSON
        self.title = "To-do List"
        json_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), "tasks.json")
        )
        self.store = JsonStore(json_path)
        self.task_counter = 0  # Compteur pour générer des clés uniques

        # Création de la disposition principale avec coins arrondis et espacement
        main_layout = RoundedColorBoxLayout(
            orientation="vertical", spacing=10, padding=10
        )

        # Création de la disposition pour l'ajout de tâches avec un champ de texte et un bouton
        input_layout = RoundedColorBoxLayout(orientation="horizontal", spacing=10)
        self.task_input = ColoredTextInput(
            hint_text="Entrer une tâche", multiline=False
        )
        add_button = ColoredButton(text="Ajouter", on_press=self.add_task)
        input_layout.add_widget(self.task_input)
        input_layout.add_widget(add_button)

        # Configuration de la couleur de fond pour la disposition des tâches "À faire"
        self.task_list_layout = RoundedColorBoxLayout(
            orientation="vertical", spacing=5, color=(0.2, 0.4, 0.6, 1)
        )
        # Configuration de la couleur de fond pour la disposition des tâches "Terminées"
        self.completed_tasks_layout = RoundedColorBoxLayout(
            orientation="vertical", spacing=5, color=(0.6, 0.2, 0.4, 1)
        )

        # Chargement des tâches existantes
        self.load_tasks()

        # Ajout des dispositions à la disposition principale
        main_layout.add_widget(input_layout)
        main_layout.add_widget(ColoredLabel(text="Tâche à faire:"))
        main_layout.add_widget(self.task_list_layout)
        main_layout.add_widget(ColoredLabel(text="Tâche finie:"))
        main_layout.add_widget(self.completed_tasks_layout)

        return main_layout

    def add_task(self, instance):
        """
        Fonction pour ajouter une nouvelle tâche à la liste.

        :param instance: Instance du bouton qui appelle la fonction.
        """
        task_text = self.task_input.text
        if task_text:
            task_checkbox = ColoredCheckBox(active=False, size_hint_x=None, width=30)
            task_label = ColoredLabel(
                text=task_text,
                halign="left",
                valign="middle",
                size_hint_x=None,
                width=150,
            )
            delete_button = ColoredButton(text="[X]", size_hint_x=None, width=70)

            delete_button.bind(on_press=self.delete_task)
            task_checkbox.bind(active=self.on_checkbox_active)

            # Configuration de la couleur de fond pour la tâche en fonction de son statut
            task_color = (
                self.task_list_layout.widget_color
            )  # Couleur par défaut pour les tâches "À faire"
            if task_checkbox.active:
                task_color = (
                    self.completed_tasks_layout.widget_color
                )  # Couleur pour les tâches terminées

            task_layout = RoundedColorBoxLayout(
                orientation="horizontal", spacing=5, color=task_color
            )
            task_layout.add_widget(task_checkbox)
            task_layout.add_widget(task_label)
            task_layout.add_widget(delete_button)

            self.task_list_layout.add_widget(task_layout)

            task_key = f"task_{self.task_counter}"
            self.task_counter += 1

            try:
                # Tentative d'écriture dans le magasin JSON
                self.store.put(task_key, text=task_text, fini=False, color=task_color)
                print(f"Tâche ajoutée: {task_text}")
            except Exception as e:
                print(f"Erreur lors de l'ajout de la tâche : {e}")

            self.task_input.text = ""

    def delete_task(self, instance):
        """
        Supprime une tâche de l'interface graphique et du magasin JSON.

        :param instance: Instance du bouton qui appelle la fonction.
        """
        # Obtient la disposition parente de l'instance (le layout de la tâche)
        task_layout = instance.parent
        # Retire la tâche des dispositions "À faire" et "Terminées"
        self.task_list_layout.remove_widget(task_layout)
        self.completed_tasks_layout.remove_widget(task_layout)

        # Parcourt les clés du magasin JSON
        for key in self.store.keys():
            # Si le texte de la tâche correspond à celui dans le magasin
            if self.store.get(key)['text'] == task_layout.children[1].text:
                # Supprime la tâche associée du magasin
                self.store.delete(key)
                break

    def on_checkbox_active(self, instance, value):
        """
        Gère l'activation/désactivation de la case à cocher d'une tâche.

        :param instance: Instance de la case à cocher qui a changé d'état.
        :param value: Nouvel état de la case à cocher (activée ou désactivée).
        """
        # Obtient la disposition parente de l'instance (le layout de la tâche)
        task_layout = instance.parent
        # Obtient le texte de la tâche à partir de la disposition
        task_text = task_layout.children[1].text
        task_key = None

        # Parcourt les clés du magasin JSON
        for key in self.store.keys():
            # Si le texte de la tâche correspond à celui dans le magasin
            if self.store.get(key)["text"] == task_text:
                # Enregistre la clé associée à la tâche
                task_key = key
                break

        # Gère le déplacement de la tâche entre les dispositions "À faire" et "Terminées"
        if value:
            self.task_list_layout.remove_widget(task_layout)
            self.completed_tasks_layout.add_widget(task_layout)
            self.store.put(task_key, text=task_text, fini=True)
        else:
            self.completed_tasks_layout.remove_widget(task_layout)
            self.task_list_layout.add_widget(task_layout)
            self.store.put(task_key, text=task_text, fini=False)

    def load_tasks(self):
        """
        Charge les tâches existantes depuis le magasin JSON et les ajoute à l'interface graphique.
        """
        # Parcourt les clés du magasin JSON
        for key in self.store.keys():
            # Obtient les valeurs associées à la clé
            value = self.store.get(key)
            # Crée une case à cocher avec l'état de la tâche
            task_checkbox = ColoredCheckBox(
                active=value["fini"], size_hint_x=None, width=30
            )
            # Crée un label avec le texte de la tâche
            task_label = ColoredLabel(
                text=value["text"],
                halign="left",
                valign="middle",
                size_hint_x=None,
                width=150,
            )
            # Crée un bouton de suppression pour la tâche
            delete_button = ColoredButton(text="[X]", size_hint_x=None, width=70)

            # Associe la fonction de suppression au bouton
            delete_button.bind(on_press=self.delete_task)
            # Associe la fonction de gestion de la case à cocher à la case à cocher
            task_checkbox.bind(active=self.on_checkbox_active)

            # Récupère et utilise la couleur de fond depuis le magasin JSON
            task_color = value.get("color", (0.2, 0.4, 0.6, 1))

            # Crée une disposition avec coins arrondis pour la tâche
            task_layout = RoundedColorBoxLayout(
                orientation="horizontal", spacing=5, color=task_color
            )
            task_layout.add_widget(task_checkbox)
            task_layout.add_widget(task_label)
            task_layout.add_widget(delete_button)

            # Ajoute la tâche à la disposition appropriée en fonction de son état
            if value["fini"]:
                self.completed_tasks_layout.add_widget(task_layout)
            else:
                self.task_list_layout.add_widget(task_layout)

if __name__ == "__main__":
    # Lance l'application To-do List
    TodoListApp().run()
