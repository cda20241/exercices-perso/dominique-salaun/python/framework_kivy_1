from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.core.window import Window

class TouchEventsApp(App):
    def build(self):
        # Crée une boîte de disposition verticale
        self.layout = BoxLayout(orientation='vertical')
        # Crée une étiquette initiale
        self.label = Label(text='Appuyer Sur une Touche...')
        # Ajoute l'étiquette à la disposition
        self.layout.add_widget(self.label)

        # Associe les événements liés au clavier
        Window.bind(on_key_down=self.on_key_down, on_key_up=self.on_key_up)

        return self.layout

    def on_key_down(self, keyboard, keycode, text, modifiers, event):
        """
        Fonction appelée lorsqu'une touche du clavier est enfoncée.

        :param keyboard: Instance du clavier
        :param keycode: Code numérique de la touche
        :param text: Caractère représentant la touche enfoncée
        :param modifiers: Liste des touches de modification enfoncées (Shift, Ctrl, etc.)
        :param event: Instance de l'événement associé à la touche enfoncée
        """
        # Traite l'événement de pression de touche
        self.label.text = f'Touche Appuyée: {keycode}, Texte: {text}, Modificateurs: {modifiers}'

    def on_key_up(self, keyboard, keycode, event):
        """
        Fonction appelée lorsqu'une touche du clavier est relâchée.

        :param keyboard: Instance du clavier
        :param keycode: Code numérique de la touche
        :param event: Instance de l'événement associé à la touche relâchée
        """
        # Traite l'événement de relâchement de touche
        self.label.text = f'Touche Relâchée: {keycode}'

if __name__ == '__main__':
    # Lance l'application TouchEventsApp
    TouchEventsApp().run()
